$(document).ready(function () {
    let keys = $('.btn');
    let calc_output = $('.calculator_output');
    let displayed_num;
    let calculate_btn = $('.calculator_calculate .btn');

    function calculate(number_1, number_2) {
        let operation = calculate_btn.data('operation');
        let result;
        switch (operation) {
            case 'add':
                result = parseFloat(number_1) + parseFloat(number_2);
                calc_output[0].value = 0;
                break;
            case 'subtract':
                result = parseFloat(number_1) - parseFloat(number_2);
                calc_output[0].value = 0;
                break;
            case 'multiply':
                result = parseFloat(number_1) * parseFloat(number_2);
                calc_output[0].value = 0;
                break;
            case 'divide':
                result = parseFloat(number_1) / parseFloat(number_2);
                calc_output[0].value = 0;
                break;
        }

        return result;
    }

    keys.click(e => {
        const key = e.target;
        const action = key.dataset.action;
        if (!action) {
            const key_content = key.textContent;
            displayed_num = calc_output[0].value;
            if (displayed_num == 0) {
                calc_output[0].value = key_content;
            } else {
                calc_output[0].value = displayed_num + key_content;
            }
        } else if (action == 'calculate') {
            let first_number = parseInt(calculate_btn.data('firstnumber'));
            let second_number = parseInt(calc_output[0].value);
            calc_output[0].value = calculate(first_number, second_number);
            calculate_btn.data('operation', 'calculated');
        } else if (action == 'decimal') {
            displayed_num = calc_output[0].value + '.';
        } else if (action == 'clear') {
            calc_output[0].value = 0;
        } else {
            calculate_btn.attr('data-firstnumber', calc_output[0].value).attr('data-operation', action);
            calc_output[0].value = 0;
        }
    })
});